function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

const WATCH_ELEMENT_CLASS = 'watch-value';
const ACTION_BTN_CLASS = 'action-btn';
const RESET_BTN_CLASS = 'reset-btn';

export class WatchView {
    #watchElement = Object.create(HTMLElement.prototype, {});
    #actionBtn = Object.create(HTMLElement.prototype, {});
    #resetBtn = Object.create(HTMLElement.prototype, {});

    get actionBtn() {
        return this.#actionBtn;
    }

    get resetBtn() {
        return this.#resetBtn;
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#watchElement = getElementByClassName(WATCH_ELEMENT_CLASS);
        this.#actionBtn = getElementByClassName(ACTION_BTN_CLASS);
        this.#resetBtn = getElementByClassName(RESET_BTN_CLASS);
    }

    updateWatchElementContent(content) {
        this.#watchElement.innerText = content;
    }

    updateActionBtnContent(content) {
        this.#actionBtn.innerText = content;
    }
}
