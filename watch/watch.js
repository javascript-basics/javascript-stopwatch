const MAX_MS_VALUE = 99;
const DEFAULT_VALUE = 0;

export class Watch {
    #seconds = DEFAULT_VALUE;
    #milliseconds = DEFAULT_VALUE;

    get seconds() {
        return this.#seconds;
    }

    set seconds(newValue) {
        this.#seconds = newValue;
    }

    get milliseconds() {
        return this.#milliseconds;
    }

    set milliseconds(newValue) {
        if (newValue === MAX_MS_VALUE) {
            newValue = DEFAULT_VALUE;
            this.#seconds++;
        }

        this.#milliseconds = newValue;
    }
}
