import {WatchView} from "./watch_view.js";
import {WatchService} from "./watch_service.js";

const START_ACTION_STR = 'Start';
const STOP_ACTION_STR = 'Stop';

export class WatchPresenter {
    #watchService = new WatchService();
    #watchView = new WatchView();
    #timer;

    constructor() {
        this.#initListeners();
    }

    #initListeners() {
        (this.#watchView.resetBtn || new HTMLElement()).addEventListener('click', this.#handleResetBtn.bind(this));
        (this.#watchView.actionBtn || new HTMLElement()).addEventListener('click', this.#handleActionBtn.bind(this));
    }

    #handleResetBtn() {
        this.#watchService.reset();
        clearInterval(this.#timer);

        this.#updateActionBtn();
        this.#updateWatch();
    }

    #handleActionBtn() {
        if (this.#watchService.stopped) {
            this.#timer = setInterval(this.#handleTimer.bind(this));
        } else {
            clearInterval(this.#timer);
        }

        this.#watchService.changeState();
        this.#updateActionBtn();
    }

    #handleTimer() {
        this.#watchService.addMillisecond();
        this.#updateWatch();
    }

    #updateActionBtn() {
        this.#watchView.updateActionBtnContent(this.#getActionBtnState());
    }

    #getActionBtnState() {
        return this.#watchService.stopped ? START_ACTION_STR : STOP_ACTION_STR;
    }

    #updateWatch() {
        this.#watchView.updateWatchElementContent(this.#watchService.formattedValue);
    }
}
