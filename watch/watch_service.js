import {Watch} from "./watch.js";

const NEXT_POINT = 10;
const ZERO_STR = '0';

const DEFAULT_VALUE = 0;

export class WatchService {
    #watch = new Watch();
    #stopped = true;

    get stopped() {
        return this.#stopped;
    }

    get formattedValue() {
        const convertToTwoNumberOfDigits = value => value < NEXT_POINT ? ZERO_STR + value : value;

        const formattedSeconds = convertToTwoNumberOfDigits(this.#watch.seconds);
        const formattedMilliseconds = convertToTwoNumberOfDigits(this.#watch.milliseconds);

        return `${formattedSeconds}:${formattedMilliseconds}`;
    }

    reset() {
        this.#watch.seconds = DEFAULT_VALUE;
        this.#watch.milliseconds = DEFAULT_VALUE;
        this.#stopped = true;
    }

    changeState() {
        this.#stopped = !this.#stopped;
    }

    addMillisecond() {
        this.#watch.milliseconds++;
    }
}
