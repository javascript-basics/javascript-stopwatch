import {WatchPresenter} from "./watch/watch_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new WatchPresenter();
        }
    }
}

Starter.start();
